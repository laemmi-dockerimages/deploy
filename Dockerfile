FROM php:7.4-cli-alpine

# Metadata
LABEL maintainer="laemmi@spacerabbit.de"

WORKDIR /var/www/html

# Packages
RUN apk update && apk add --no-cache \
    openssh \
    rsync \
# Composer
    && curl --silent --show-error -LO https://getcomposer.org/installer \
    && php installer --install-dir=/usr/local/bin --filename=composer \
    && chmod +x /usr/local/bin/composer \
# Deployer global
    && curl --silent --show-error -LO https://deployer.org/deployer.phar \
    && mv deployer.phar /usr/local/bin/dep \
    && chmod +x /usr/local/bin/dep

COPY ./composer* ./

RUN composer install --prefer-dist --no-progress --no-suggest --no-ansi --no-interaction --optimize-autoloader